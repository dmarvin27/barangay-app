importScripts("https://www.gstatic.com/firebasejs/8.0.0/firebase-app.js")

importScripts("https://www.gstatic.com/firebasejs/8.0.0/firebase-analytics.js")

importScripts("https://www.gstatic.com/firebasejs/8.0.0/firebase-messaging.js")
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyBCwOFiyCBmLg5lIJnImvZUnP3Zn--67-s",
    authDomain: "barangaysystem-69ff1.firebaseapp.com",
    databaseURL: "https://barangaysystem-69ff1.firebaseio.com",
    projectId: "barangaysystem-69ff1",
    storageBucket: "barangaysystem-69ff1.appspot.com",
    messagingSenderId: "201404288722",
    appId: "1:201404288722:web:91e627a0717aa4ccd0670b",
    measurementId: "G-2VZMPQTZJ2"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  const messaging = firebase.messaging();

  messaging.onBackgroundMessage(function(payload){
    const notificationTitle = payload.notification.title;
    const notificationOptions = {
      body: payload.notification.body
    };
    // console.log(payload.notification.title)
    return self.registration.showNotification(notificationTitle,notificationOptions);
  })