import Vue from 'vue'
import VueRouter from 'vue-router'
import CreateAnnouncement from '../views/CreateAnnouncement'
import AnnouncementCard from '../components/AnnouncementCard'
import About from '../views/About'
Vue.use(VueRouter)


const routes = [
  {
    path: '/CreateAnnouncement',
    name: 'CreateAnnouncement',
    component: CreateAnnouncement
  },
  {
    path:'/AnnouncementCard',
    name: 'AnnouncementCard',
    component: AnnouncementCard
  },
  {
    path:'/About',
    name:'About',
    component: About
  }
]

const router = new VueRouter({
  routes
})

export default router