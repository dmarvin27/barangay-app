import firebase from 'firebase'
import 'firebase/app'

  var firebaseConfig = {
    apiKey: "AIzaSyBCwOFiyCBmLg5lIJnImvZUnP3Zn--67-s",
    authDomain: "barangaysystem-69ff1.firebaseapp.com",
    databaseURL: "https://barangaysystem-69ff1.firebaseio.com",
    projectId: "barangaysystem-69ff1",
    storageBucket: "barangaysystem-69ff1.appspot.com",
    messagingSenderId: "201404288722",
    appId: "1:201404288722:web:91e627a0717aa4ccd0670b",
    measurementId: "G-2VZMPQTZJ2"  
  };

 const vuebase = firebase.initializeApp(firebaseConfig);

export default vuebase
