import Vue from 'vue'
import Vuex from 'vuex'
import firebase from "../firebase"

Vue.use(Vuex)
Vue.use(firebase);

export const store = new Vuex.Store({
  state: {
    AnnouncementDetails: []
  },
  mutations: {
    getAnnouncementDetails(state, val) {
      state.AnnouncementDetails = val
    }
  },
  actions: {
    clearData({ commit }) {
      commit('getAnnouncementDetails', null)
    },
    getAnnouncementDetails({ commit }) {
      firebase.database().ref('Barangay/7ivdJvY8c3WMSlHdkwty/Announcements').on('value', snapshot => {
        let returnArr = []
        snapshot.forEach(function (childSnapshot) {
          returnArr.push(childSnapshot.val())
        });
        commit('getAnnouncementDetails', returnArr)
      })
    }
  },
  modules: {

  },
  getters: {
    countAnnouncements: state => {
      return state.AnnouncementDetails.length
    }
  }
})
